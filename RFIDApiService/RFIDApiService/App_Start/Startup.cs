﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RFIDApiService.Providers;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin;
using Owin;

namespace RFIDApiService
{
    public partial class Startup
    {

        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }
        static Startup()
        {
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/token"),
                Provider = new OAuthProvider(new AccountDAO()),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(8),
                AllowInsecureHttp = true
            };
        }
        public void ConfigureAuth(IAppBuilder app)
        {
            app.UseOAuthBearerTokens(OAuthOptions);
        }
    }
}