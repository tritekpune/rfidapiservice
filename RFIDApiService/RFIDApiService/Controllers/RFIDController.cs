﻿using RFIDApiService.Models;
using RfidEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RFIDApiService.Controllers
{
    [RoutePrefix("api/rfid")]
    public class RFIDController : BaseController
    {
        [HttpPost]
        [Authorize]
        [Route("saverfid")]
        public IHttpActionResult SaveRFID(RFID RfidDetails)
        {
            try
            {
                string result = rfidDAO.SaveRFIDDetails(RfidDetails);
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        [Authorize]
        [Route("updaterfid")]
        public IHttpActionResult UpdateRFID(RFID RfidDetails)
        {
            try
            {
                String result = rfidDAO.UpdateRFIDDetails(RfidDetails);
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        [Route("GetSuppDetailsForRFID")]
        public List<SuppRegion> GetSuppDetailsForRFID()
        {
            return rfidDAO.GetSuppDetailsForRFID();
        }


        [HttpGet]
        [Authorize]
        [Route("GetRfidList")]
        public IHttpActionResult GetRFIDList()
        {
            try
            {
                List<RFIDModel> rfid = rfidDAO.GetRFIDList();
                if (rfid!=null)
                    return Ok(rfid);
                else
                    return BadRequest("Error getting RFID list.");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        [Route("report/rfid")]
        public IHttpActionResult GetRFIDDetails()
        {
            try
            {
                return Ok(rfidDAO.GetRFIDList());
            }
            catch (Exception)
            {

                throw;
            }
        }


    }
}
