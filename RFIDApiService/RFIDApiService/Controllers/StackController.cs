﻿using RfidEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RFIDApiService.Controllers
{
    [RoutePrefix("api/stack")]
    public class StackController : BaseController
    {
        [HttpGet]
        [Authorize]
        [Route("supList")]
        public IHttpActionResult GetSupList()
        {
            try
            {
                dynamic supList = supplierDAO.GetSupList();
                if (supList != null)
                    return Ok(supList);
                else
                    return BadRequest("Error fetching supplier list.");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //[HttpPost]
        //[Authorize]
        //[Route("savestack")]
        //public IHttpActionResult SaveStack(StackDetail stackDetails)
        //{
        //    try
        //    {
        //        if (stackDAO.SaveStackDetails(stackDetails))
        //            return Ok("Stack details saved.");
        //        else
        //            return BadRequest("Error fetching supplier list.");
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}


        [HttpGet]
        [Authorize]
        [Route("GetStackDetails")]
        public IHttpActionResult GetStackDetails()
        {
            try
            {
                List<StackScanTokenDetailModel> stackList = stackDAO.GetStackDetailsList();
                if (stackList != null)
                    return Ok(stackList);
                else
                    return BadRequest("Error fetching stack details.");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// save barcode against the stack
        /// </summary>
        /// <param name="stackBarcode"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("SaveStackBarcode")]
        public IHttpActionResult SaveStackBarcode(StackScanTokenDetailModel stackBarcode)
        {
            return Ok(stackDAO.SaveStackBarcode(stackBarcode));
        }


        /// <summary>
        /// search using token generated from mobile application and display list of scanned RFID's
        /// </summary>
        /// <param name="stackScanTokenDetails"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("SearchToken")]
        public IHttpActionResult SearchStackToken(StackScanTokenDetailModel stackScanTokenDetails)
        {
            try
            {
                List<RFIDScanDetailModel> rfidDetails = stackDAO.SearchStackToken(stackScanTokenDetails);
                if (rfidDetails != null)
                    return Ok(rfidDetails);
                else
                    return BadRequest("Invalid Token");
            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpPost]
        [Authorize]
        [Route("GetStackBarcode")]

        public IHttpActionResult GetStackBarcode(StackScanTokenDetail stack)
        {
            try
            {
                return Ok(accountDAO.GetStackBarcode(stack.StackScanId));
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        [HttpGet]
        [Authorize]
        [Route("report/supplierwisestack")]
        public IHttpActionResult GetSupplierWiseStackDetails()
        {
            try
            {
                return Ok(stackDAO.GetSupplierWiseStackDetails());
            }
            catch (Exception)
            {

                throw;
            }
        }


        [HttpGet]
        [Authorize]
        [Route("report/UserWiseStack")]
        public IHttpActionResult GetUserWiseStackDetails()
        {
            try
            {
                return Ok(stackDAO.GetUserWiseStackDetails());
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
