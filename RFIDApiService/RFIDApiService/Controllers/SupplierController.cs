﻿using Newtonsoft.Json.Linq;
using RFIDApiService.Models;
using RfidEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.WebPages.Html;

namespace RFIDApiService.Controllers
{
    [RoutePrefix("api/Supplier")]
    public class SupplierController : BaseController
    {

        [HttpPost]
        //[Authorize(Roles = "admin")]
        [Authorize]
        [Route("SaveSupplier")]
        public IHttpActionResult SaveSupplier(JObject param)
        {
            Supplier supplier = param["supplier"].ToObject<Supplier>();
            Region region = param["region"].ToObject<Region>();
            if (supplier != null && region !=null)
            {
                //Add supplier first and then corresponding region
                if (supplierDAO.AddSupplier(supplier, region))
                    return Ok("Supplier saved.");
                else
                    return Ok("Duplicate or error saving supplier");

            }
            return Ok();
        }


        [HttpGet]
        [Authorize]
        [Route("countryandregionlist")]
        public CountryAndRegion GetCountryList()
        {
            return supplierDAO.GetCountryRegionList();
        }

        [HttpGet]
        [Authorize]
        [Route("regionlist")]
        public IEnumerable<Region> GetRegionList()
        {
            return supplierDAO.GetRegionList();
        }

        [HttpGet]
        [Authorize]
        [Route("GetSuppDetails")]
        public List<SuppRegion> GetSuppDetails()
        {
            return supplierDAO.GetSupRegList();
        }

        [HttpPost]
        [Authorize]
        [Route("editSupplierDetails")]
        public IHttpActionResult EditSupplierDetails(SuppRegion supplier)
        {
            if (supplierDAO.UpdateSupplier(supplier))
                return Ok("Supplier updated.");
            else
                return BadRequest("Error updating supplier");
        }


        [HttpGet]
        [Authorize]
        [Route("report/suppliersummary")]
        public IHttpActionResult GetSupplierSummaryReport()
        {
            try
            {
                return Ok(supplierDAO.GetSupRegList());
            }
            catch (Exception)
            {

                throw;
            }
           
        }
            
    }
}