﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using RfidEntities;
using System.Web;

namespace RFIDApiService.Controllers
{
    public class BaseController : ApiController
    {
        private RFIDEntities db;
        protected AccountDAO accountDAO;
        protected SupplierDAO supplierDAO;
        protected StackDAO stackDAO;
        protected RfidDAO rfidDAO;
        public BaseController()
        {
            db = new RFIDEntities();
            accountDAO = new AccountDAO(db,new MD5PasswordHasher());
            supplierDAO = new SupplierDAO(db);
            stackDAO = new StackDAO(db);
            rfidDAO = new RfidDAO(db);
        }

               
    }
}
