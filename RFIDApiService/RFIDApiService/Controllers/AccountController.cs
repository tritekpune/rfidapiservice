﻿using Newtonsoft.Json.Linq;
using RFIDApiService.Models;
using RfidEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RFIDApiService.Controllers
{
    [RoutePrefix("api/account")]
    public class AccountController : BaseController
    {
        // POST: api/account/register
        [HttpPost]
        [Route("register")]
        public IHttpActionResult Register(User registerUser)
        {
            try
            {
                // Validating Username 
                if (accountDAO.ValidateUsername(registerUser))
                {
                    ModelState.AddModelError("", "User is Already Registered");
                    return BadRequest(ModelState);
                }

                // Saving User Details in Database
                if (accountDAO.Add(registerUser))
                    return Ok("User registered successfully.");
                else
                    return BadRequest("User not added. Please contact administrator");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }


        [HttpPost]
        [Route("editrole")]
        [Authorize]
        public IHttpActionResult EditRole(JObject roleObject)
        {
            try
            {
                String roleName = roleObject["RoleName"].ToObject<string>();
                String emailId = roleObject["EmailId"].ToObject<string>();
                String contact = roleObject["Contact"].ToObject<string>();
                String CountryCode = roleObject["CountryCode"].ToObject<string>();
                bool isActive = roleObject["IsActive"].ToObject<bool>();
                // Saving User role in Database
                if (accountDAO.EditRole(roleName, emailId, contact, isActive, CountryCode))
                {
                    ModelState.Clear();
                    return Ok("User updated successfully");
                }
                else
                    return BadRequest("Error updating role.");
            }
            catch (Exception ex)
            {

                throw;
            }
           
        }


        [HttpGet]
        [Authorize]
        [Route("getuserroles")]
        public IHttpActionResult GetUserRole()
        {
            try
            {
                dynamic userrole =  accountDAO.GetUserRole();
                if (userrole != null)
                    return Ok(userrole);
                else
                    return BadRequest("Error fetching role.");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpGet]
        [Authorize]
        [Route("roles")]
        public IHttpActionResult GetRoles()
        {
            try
            {
                List<Role> roles = accountDAO.GetRoles();
                if (roles != null)
                    return Ok(roles);
                else
                      return BadRequest("Error fetching role.");
            }
            catch (Exception)
            {

                throw;
            }
        }


        [HttpPost]
        [Route("finduser")]
        public IHttpActionResult FindUser(PasswordToken passToken)
        {
            try
            {
                PasswordToken token = accountDAO.FindUser(passToken.EmailId);
                if (token != null)
                    return Ok(token);
                else
                    return Ok("User not found");
            }
            catch (Exception)
            {
                throw;
            }
        }


        [HttpPost]
        [Route("resetpassword")]
        public IHttpActionResult ResetPassword(ResetPassword resetPassword)
        {
            try
            {
                if(accountDAO.ResetPassword(resetPassword))
                    return Ok("Password updated Successfully");
                else
                    return Ok("User not found");
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        [Authorize]
        [Route("SaveUser")]
        public IHttpActionResult SaveUser(UserPlaceholder user)
        {
            try
            {
                return Ok(accountDAO.SaveUserPlaceHolder(user));
            }
            catch (Exception)
            {

                return BadRequest("Error saving user details");
            }
        }

        [HttpGet]
       [Authorize]
        [Route("unregisteruserlist")]
        public IHttpActionResult GetUnRegisteredUserList()
        {
            try
            {
                dynamic userList = accountDAO.GetUnRegisteredUsers();
                if (userList != null)
                    return Ok(userList);
                else
                    return BadRequest("Error fetching users.");
            }
            catch (Exception)
            {

                throw;
            }
        }


        [HttpGet]
        [Authorize]
        [Route("report/userDetails")]
        public IHttpActionResult GetUserDetails()
        {
            try
            {
                return Ok(accountDAO.GetUserList());
            }
            catch (Exception)
            {

                throw;
            }
        }


    }
}
