﻿using System;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Owin.Security.OAuth;
using System.Security.Principal;
using System.Threading;
using System.Web;

namespace RFIDApiService.Providers
{
    public class OAuthProvider: OAuthAuthorizationServerProvider
    {
        private AccountDAO accountDAO;
        public OAuthProvider(AccountDAO acntdao)
        {
            accountDAO = acntdao;
        }
        
        #region[GrantResourceOwnerCredentials]
        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            return Task.Factory.StartNew(() =>
            {
                var userName = context.UserName;
                var password = context.Password;
                var user = accountDAO.ValidateRegisteredUser(userName, password);
                if (user != null && user.isActive)
                {
                    var claims = new List<Claim>()
                    {
                        new Claim(ClaimTypes.Sid, Convert.ToString(user.userId)),
                        new Claim(ClaimTypes.Name, user.userName),
                        new Claim(ClaimTypes.Email, user.EmailId)
                    };
                    ClaimsIdentity oAuthIdentity = new ClaimsIdentity(claims,
                                Startup.OAuthOptions.AuthenticationType);

                    var properties = CreateProperties(user.userName, user.RoleName, user.FirstName + " " + user.LastName);
                    var ticket = new AuthenticationTicket(oAuthIdentity, properties);
                    context.Validated(ticket);
                }
                else if (user != null && !user.isActive)
                    context.SetError("invalid_grant", "Unauthorized access. Please contact administrator");
                else
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect");
                }
            });
        }
        #endregion

        #region[ValidateClientAuthentication]
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            if (context.ClientId == null)
                context.Validated();

            return Task.FromResult<object>(null);
        }
        #endregion

        #region[TokenEndpoint]
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
                
            }

            return Task.FromResult<object>(null);
        }
        #endregion

        #region[CreateProperties]
        public static AuthenticationProperties CreateProperties(string userName,string role,string UserFullName)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName },
                { "Role",role},  //we can add role of the user here
                { "UserFullName",UserFullName}
            };
            return new AuthenticationProperties(data);
        }
        #endregion
    }
}
