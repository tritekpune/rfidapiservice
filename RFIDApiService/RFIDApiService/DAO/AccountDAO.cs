﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RfidEntities;
using System.Security.Principal;
using System.Threading;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Data.Entity;
using RFIDApiService.Models;

namespace RFIDApiService
{
    public class AccountDAO : BaseDAO
    {
        public AccountDAO() { }

        protected IEnumerable<Claim> claims;

        public AccountDAO(RFIDEntities db, MD5PasswordHasher md5)
            : base(db, md5)
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            claims = identity.Claims;
        }


        public bool Add(User registeruser)
        {
            try
            {
                UserPlaceholder user = r_db.UserPlaceholders.Find(registeruser.UserEmailId);
                if (user != null && user.IsClosed==false)
                {
                    registeruser.UserAddDt = DateTime.Now;
                    registeruser.UserIsActive = true;
                    registeruser.UserAddBy = r_db.Users.Where(x=>x.UserId== user.UserAddBy).FirstOrDefault().UserEmailId;
                    registeruser.Role = r_db.Roles.FirstOrDefault(r => r.RoleId == 1);

                    // Encrypting Password with AES 256 Algorithm
                    //registeruser.Password = AESEncryption.AESEncryption.EncryptText(registeruser.Password);
                    registeruser.Password = md5.HashPassword(registeruser.Password);
                    registeruser.UserContact = user.Contact;
                    registeruser.UserCountryCode = user.CountryCode;
                    r_db.Users.Add(registeruser);

                    //inactive the user entry from user placeholder table.
                    user.IsClosed = true;
                    r_db.Entry(user).State = EntityState.Modified;
                    r_db.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public bool EditRole(string roleName,string emailId,string contact,bool isActive,string CountryCode)
        {
            try
            {
                var updateUser = r_db.Users.Where(u => u.UserEmailId == emailId).FirstOrDefault();
                updateUser.UserRole = r_db.Roles.FirstOrDefault(u => u.RoleName == roleName).RoleId ;
                updateUser.UserContact = contact;
                updateUser.UserCountryCode = CountryCode;
                updateUser.UserIsActive = isActive;
                updateUser.UserModfDt = DateTime.Now;
                updateUser.UserModfBy= claims.Where(x => x.Type == ClaimTypes.Email).FirstOrDefault().Value;
                r_db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        internal List<Role> GetRoles ()
        {
            var roles = r_db.Roles;
            return roles.ToList<Role>();
        }

        internal dynamic GetUserRole()
        {
            dynamic userrole = (from user in r_db.Users
                                join usrrole in r_db.Roles
                                on user.UserRole equals usrrole.RoleId
                                select new {
                                    userId = user.UserId,
                                    FirstName = user.UserFirstName,
                                    LastName = user.UserLastName,
                                    EmailId = user.UserEmailId,
                                    RoleId = usrrole.RoleId,
                                    RoleName = usrrole.RoleName,
                                    Contact=user.UserContact,
                                    CountryCode= user.UserCountryCode,
                                    ContactWithCode =user.UserCountryCode + user.UserContact,
                                    AddDt = user.UserAddDt,
                                    IsActive=user.UserIsActive,
                                    AddBy = r_db.Users.Where(x => x.UserEmailId == user.UserAddBy).FirstOrDefault().UserFirstName + " " + r_db.Users.Where(x => x.UserEmailId == user.UserAddBy).FirstOrDefault().UserLastName,
                                    ModfDt=user.UserModfDt,
                                    ModfBy= r_db.Users.Where(x => x.UserEmailId == user.UserModfBy).FirstOrDefault().UserFirstName + " " + r_db.Users.Where(x => x.UserEmailId == user.UserModfBy).FirstOrDefault().UserLastName,
                                });
            return userrole;
        }


        public AuthUser ValidateRegisteredUser(string userName,string password)
        {
            try
            {
                string hashPassword = md5.HashPassword(password);
                //var user =  r_db.Users.FirstOrDefault(u => u.UserName == userName && u.Password == hashPassword);
                AuthUser user = (from u in r_db.Users
                                    join role in r_db.Roles
                                    on u.UserRole equals role.RoleId
                                    where(u.UserName==userName && u.Password==hashPassword)
                                    select new AuthUser
                                    {
                                        userId = u.UserId,
                                        userName=u.UserName,
                                        FirstName = u.UserFirstName,
                                        LastName = u.UserLastName,
                                        EmailId = u.UserEmailId,
                                        RoleName = role.RoleName,
                                        isActive=u.UserIsActive
                                    }).FirstOrDefault();
                if (user!=null)
                {
                    return user;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public bool ValidateUsername(User registeruser)
        {
            try
            {
                var usercount = (from User in r_db.Users
                                 where User.UserName == registeruser.UserName || User.UserEmailId==registeruser.UserEmailId
                                 select User).Count();
                if (usercount > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }


        public bool ValidateEmail(User user)
        {
            try
            {
                var usercount = (from User in r_db.Users
                                 where User.UserEmailId == user.UserEmailId
                                 select User).Count();
                if (usercount > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }


        internal PasswordToken FindUser(string emailId)
        {
            try
            {
                User user = new User();
                PasswordToken token = new PasswordToken();
                user.UserEmailId = emailId;
                //Generate Token based on user
                if (ValidateEmail(user))
                {
                    token.EmailId = emailId;
                    token.Token = GeneratePassword();
                    token.CreatedDate = DateTime.Now;
                    token.IsActive = true;
                    CheskUserHasToken(token.EmailId);
                    r_db.PasswordTokens.Add(token);
                    r_db.SaveChanges();
                }
                return token;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private bool CheskUserHasToken(string emailId)
        {
            PasswordToken passToken = r_db.PasswordTokens.Where(tok => tok.EmailId == emailId).FirstOrDefault();
            if (passToken != null)
            {
                r_db.Entry(passToken).State = EntityState.Deleted;
                r_db.SaveChanges();
                return true;
            }
            return false;
        }

        private string GeneratePassword()
        {
            string strPwdchar = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string strPwd = "";
            Random rnd = new Random();
            for (int i = 0; i <= 7; i++)
            {
                int iRandom = rnd.Next(0, strPwdchar.Length - 1);
                strPwd += strPwdchar.Substring(iRandom, 1);
            }
            return strPwd;
        }


        internal bool ResetPassword(ResetPassword resetPassword)
        {
            try
            {
                if (ValidateToken(resetPassword))
                {
                    string hashPassword = md5.HashPassword(resetPassword.Password);
                    User user = r_db.Users.FirstOrDefault(u => u.UserEmailId == resetPassword.Email);
                    if (user != null)
                    {
                        user.Password = hashPassword;
                        r_db.Entry(user).State = EntityState.Modified;
                        r_db.SaveChanges();
                        //after password reset delete the token

                        PasswordToken passToken = r_db.PasswordTokens.Where(tok => tok.EmailId == resetPassword.Email).FirstOrDefault();
                        if (passToken != null)
                        {
                            r_db.Entry(passToken).State = EntityState.Deleted;
                            r_db.SaveChanges();
                        }

                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception)
            {

                throw;
            }
        }

            private bool ValidateToken(ResetPassword resetPassword)
        {
            try
            {
                PasswordToken result = r_db.PasswordTokens.Where(x => x.EmailId == resetPassword.Email && x.Token == resetPassword.ReturnToken).FirstOrDefault();
                if (result != null)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {

                throw;
            }
        }


        internal string SaveUserPlaceHolder(UserPlaceholder user)
        {
            try
            {
                if (!CheckuserExists(user))
                {
                    user.UserAddDt = DateTime.Now;
                    user.UserAddBy = Convert.ToInt32(claims.Where(x => x.Type == ClaimTypes.Sid).FirstOrDefault().Value);
                    user.IsClosed = false;
                    r_db.UserPlaceholders.Add(user);
                    r_db.SaveChanges();
                    return "User added successfully.";
                }
                else
                    return "User already exists.";
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        internal bool CheckuserExists(UserPlaceholder user)
        {
            UserPlaceholder usr = r_db.UserPlaceholders.Where(u=>u.UserEmailId==user.UserEmailId).AsNoTracking().FirstOrDefault();
            if (usr == null)
            {
                User regUser = r_db.Users.Where(u => u.UserEmailId == user.UserEmailId).FirstOrDefault();
                if (regUser != null)
                    return true;
                else
                    return false;
            }
            else
                return true;
        }

        internal dynamic GetUnRegisteredUsers()
        {
            try
            {
                dynamic result = (from user in r_db.UserPlaceholders.AsNoTracking()
                                           where user.IsClosed==false
                                           select new
                                           {
                                               Id=user.Id,
                                               UserEmailId=user.UserEmailId,
                                               RoleId=user.RoleId,
                                               RoleName=r_db.Roles.Where(r=>r.RoleId==user.RoleId).FirstOrDefault().RoleName,
                                               CountryCode=user.CountryCode,
                                               Contact=user.Contact,
                                               ContactWithCode=user.CountryCode+user.Contact,
                                               Status =user.Status,
                                               UserAddBy=user.UserAddBy,
                                               UserAddByName= r_db.Users.Where(x => x.UserId == user.UserAddBy).FirstOrDefault().UserFirstName + " " + r_db.Users.Where(x => x.UserId == user.UserAddBy).FirstOrDefault().UserLastName,
                                               UserAddDt=user.UserAddDt,
                                               UserModfBy=user.UserModfBy,
                                               UserModfByName = r_db.Users.Where(x => x.UserId == user.UserModfBy).FirstOrDefault().UserFirstName + " " + r_db.Users.Where(x => x.UserId == user.UserModfBy).FirstOrDefault().UserLastName,
                                               UserModfDt =user.UserModfDt
                                           }).ToList();
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }


        internal object GetStackBarcode(int stackID)
        {
            try
            {
                StackScanTokenDetail stackDetails = r_db.StackScanTokenDetails.Where(stack => stack.StackScanId == stackID).FirstOrDefault();
                return stackDetails;
            }
            catch (Exception)
            {

                throw;
            }
        }


        internal dynamic GetUserList()
        {
            try
            {
                dynamic userList = (from user in r_db.Users
                                       join usrrole in r_db.Roles
                                       on user.UserRole equals usrrole.RoleId
                                       select new 
                                       {
                                           UserId = user.UserId,
                                           UserFirstName = user.UserFirstName,
                                           UserLastName = user.UserLastName,
                                           UserEmailId = user.UserEmailId,
                                           RoleId = usrrole.RoleId,
                                           RoleName = usrrole.RoleName,
                                           UserContact = user.UserContact,
                                           CountryCode=user.UserCountryCode,
                                           ContactWithCode=user.UserCountryCode+user.UserContact,
                                           UserAddDt = user.UserAddDt,
                                           UserAddByName = r_db.Users.Where(x => x.UserEmailId == user.UserAddBy).FirstOrDefault().UserFirstName + " " + r_db.Users.Where(x => x.UserEmailId == user.UserAddBy).FirstOrDefault().UserLastName,
                                           UserModfDt = user.UserModfDt,
                                           ModfByName = r_db.Users.Where(x => x.UserEmailId == user.UserModfBy).FirstOrDefault().UserFirstName + " " + r_db.Users.Where(x => x.UserEmailId == user.UserModfBy).FirstOrDefault().UserLastName,
                                           UserIsActive= user.UserIsActive,
                                           UserName=user.UserName
                                       });
                return userList;
            }
            catch (Exception)
            {

                throw;
            }
        }



        //private string GenerateToken(User user)
        //{
        //    try
        //    {
        //        //Generate Token based on user
        //        string role = GetuserRole(user);

        //        string randomnumber =
        //           string.Join(":", new string[]
        //           {   Convert.ToString(user.UserName),
        //        Convert.ToString( role),
        //        Convert.ToString(DateTime.Now.ToShortDateString()),
        //           });

        //        string token = AESEncryption.AESEncryption.EncryptText(randomnumber);
        //        return token;

        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}


        //private void SetPrincipal(IPrincipal principal)
        //{

        //    Thread.CurrentPrincipal = principal;
        //    if (HttpContext.Current != null)
        //    {
        //        HttpContext.Current.User = principal;
        //    }
        //}

        //private string GetuserRole(User user)
        //{
        //    return r_db.Roles.FirstOrDefault(r => r.RoleId == user.UserRole).RoleName.ToString();
        //}
    }
}