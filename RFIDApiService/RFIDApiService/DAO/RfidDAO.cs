﻿using RfidEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using System.Threading;
using RFIDApiService.Models;

namespace RFIDApiService
{
    public class RfidDAO: BaseDAO
    {
        public RfidDAO() { }

        protected IEnumerable<Claim> claims;
        public RfidDAO(RFIDEntities db)
            : base(db)
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            claims = identity.Claims;
        }

        internal String SaveRFIDDetails(RFID rfidDetails)
        {
            try
            {
                String retVal = String.Empty;
                if (!CheckRFIDExists(rfidDetails))
                {
                    rfidDetails.RFIDAddDt = DateTime.Now;
                    rfidDetails.RFIDSupplierName = r_db.Suppliers.Where(supp => supp.SupplierCode == rfidDetails.RFIDSupplierCode).FirstOrDefault().SupplierName;
                    rfidDetails.RFIDAddBy = Convert.ToInt32(claims.Where(x => x.Type == ClaimTypes.Sid).FirstOrDefault().Value);
                    r_db.RFIDs.Add(rfidDetails);
                    r_db.SaveChanges();
                    retVal = "RFID details saved successfully";
                }
                else
                    retVal = "RFID already Exists.";
                return retVal;
            }
            catch (Exception)
            {

                throw;
            }
           
        }

       

        private bool CheckRFIDExists(RFID rfidDetails)
        {
            var result = r_db.RFIDs.AsNoTracking().Where(rfid => rfid.RFIDCode == rfidDetails.RFIDCode);
            if (result.Count() > 0)
                return true;
            else
                return false;
        }

       
        internal String UpdateRFIDDetails(RFID rfidDetails)
        {
            try
            {
                //if (!CheckRFIDExists(rfidDetails))
                //{
                    RFID rfid = r_db.RFIDs.Find(rfidDetails.RFIDId);
                    rfid.RFIDCode = rfidDetails.RFIDCode;
                    rfid.RFIDComments = rfidDetails.RFIDComments;
                    rfid.RFIDIsActive = rfidDetails.RFIDIsActive;
                    rfidDetails.RFIDSupplierName = r_db.Suppliers.Where(supp => supp.SupplierCode == rfidDetails.RFIDSupplierCode).FirstOrDefault().SupplierName;
                    rfid.RFIDModfDt = DateTime.Now;
                    rfid.RFIDModfBy = Convert.ToInt32(claims.Where(x => x.Type == ClaimTypes.Sid).FirstOrDefault().Value);
                    r_db.Entry(rfid).State = System.Data.Entity.EntityState.Modified;
                    r_db.SaveChanges();
                    return "RFID details updated successfully";
                //}
                //else
                //{
                //    return "RFID already Exists";
                //}
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        internal List<RFIDModel> GetRFIDList()
        {
            //Modify this query to get addbyname and modfbyname in proper way.
            List<RFIDModel> RfidList = (from rfid in r_db.RFIDs
                           select new RFIDModel
                           {
                               RFIDId = rfid.RFIDId,
                               RFIDCode = rfid.RFIDCode,
                               RFIDSupplierName = r_db.Suppliers.Where(supp => supp.SupplierCode == rfid.RFIDSupplierCode).FirstOrDefault().SupplierName,
                               RFIDSupplierCode = rfid.RFIDSupplierCode,
                               RFIDIsActive = rfid.RFIDIsActive,
                               RFIDComments = rfid.RFIDComments,
                               RFIDAddDt = rfid.RFIDAddDt,
                               RFIDAddBy = rfid.RFIDAddBy,
                               RFIDAddByName = r_db.Users.Where(x => x.UserId == rfid.RFIDAddBy).FirstOrDefault().UserFirstName + " " + r_db.Users.Where(x => x.UserId == rfid.RFIDAddBy).FirstOrDefault().UserLastName,
                               RFIDModfDt = rfid.RFIDModfDt,
                               RFIDModfBy = rfid.RFIDModfBy,
                               RFIDModfByName = r_db.Users.Where(x => x.UserId == rfid.RFIDModfBy).FirstOrDefault().UserFirstName + " " + r_db.Users.Where(x => x.UserId == rfid.RFIDModfBy).FirstOrDefault().UserLastName,
                           }).ToList();
            return RfidList;
        }

        internal List<SuppRegion> GetSuppDetailsForRFID()
        {
            List<SuppRegion> suppReg = (from sup in r_db.Suppliers
                                        join supreg in r_db.SupplierRegionMappings
                                        on sup.SupplierCode equals supreg.SupplierCode
                                        join reg in r_db.Regions
                                        on supreg.RegionID equals reg.RegionID
                                        where sup.SupplierIsActive==true
                                        select new SuppRegion
                                        {
                                            SupplierCode = sup.SupplierCode,
                                            SupplierName = sup.SupplierName,
                                            SupplierEmailId = sup.SupplierEmailId,
                                            SupplierContact = sup.SupplierContact,
                                            SupplierAddress1 = sup.SupplierAddress1,
                                            SupplierAddress2 = sup.SupplierAddress2,
                                            PinCode = sup.PinCode,
                                            City = sup.City,
                                            State = sup.State,
                                            CountryCode = sup.CountryCode,
                                            Country = sup.Country,
                                            SupplierIsActive = sup.SupplierIsActive,
                                            SupplierRegion = reg.RegionName,
                                            SupplierAddBy = sup.SupplierAddBy,
                                            SupplierAddDt = sup.SupplierAddDt,
                                            SupplierModfBy = sup.SupplierModfBy,
                                            SupplierModfDt = sup.SupplierModfDt,
                                            SupplierAddByName = r_db.Users.Where(x => x.UserId == sup.SupplierAddBy).FirstOrDefault().UserFirstName + " " + r_db.Users.Where(x => x.UserId == sup.SupplierAddBy).FirstOrDefault().UserLastName,
                                            SupplierModfByName = r_db.Users.Where(x => x.UserId == sup.SupplierModfBy).FirstOrDefault().UserFirstName + " " + r_db.Users.Where(x => x.UserId == sup.SupplierModfBy).FirstOrDefault().UserLastName,
                                        }).ToList();
            return suppReg;
        }
    }
}