﻿using RFIDApiService.Models;
using RfidEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.WebPages.Html;

namespace RFIDApiService
{
    public class SupplierDAO: BaseDAO
    {

        public SupplierDAO() { }

        protected IEnumerable<Claim> claims;
        public SupplierDAO(RFIDEntities db)
            : base(db)
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            claims = identity.Claims;
        }

        public bool AddSupplier(Supplier supplier,Region region)
        {
            try
            {
                //check if supplier already exists based on emailid and create supplier code
                //created int supplier code for now
                int supcode =CheckSupplierCode(supplier);
                if (supcode == 0)
                    return false;
                else
                {
                    supplier.SupplierCode = Convert.ToString(supcode);
                    supplier.SupplierAddDt = DateTime.Now;
                    supplier.SupplierAddBy= Convert.ToInt32(claims.Where(x => x.Type == ClaimTypes.Sid).FirstOrDefault().Value); 
                    r_db.Suppliers.Add(supplier);

                    //Add supplier region mapping
                    SupplierRegionMapping supregion = new SupplierRegionMapping();
                    supregion.RegionID = r_db.Regions.Where(x=>x.RegionName==region.RegionName).FirstOrDefault().RegionID;
                    supregion.SupplierCode = supplier.SupplierCode;
                    r_db.SupplierRegionMappings.Add(supregion);
                    r_db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            { throw ex; }
        }

        internal dynamic GetSupList()
        {
            List<Supplier> supplier = new List<Supplier>();
            supplier = r_db.Suppliers.ToList<Supplier>();
            return supplier;
        }

       

        public int CheckSupplierCode(Supplier supplier) {
            try
            {
                int result=0;
                if (!SupplierExists(supplier.SupplierEmailId))
                {
                    int baseSuppCode = 1001;
                    var highestSupCode = r_db.Suppliers.AsEnumerable().Max(x => x.SupplierCode);
                    if (highestSupCode == null)
                        result= baseSuppCode;
                    else
                        result= Convert.ToInt32(highestSupCode) + 1;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

       

        internal IEnumerable<Region> GetRegionList()
        {
            var regionList = r_db.Regions.AsEnumerable();
            return regionList;
        }

        private bool SupplierExists(string emailID)
        {
            var result = r_db.Suppliers.AsNoTracking().Where(x => x.SupplierEmailId.ToLower() == emailID.ToLower());
            if (result.Count() > 0)
                return true;
            else
                return false;
        }

        public CountryAndRegion GetCountryRegionList()
        {
            //commented country list as it will be populated from web application.
            //var countryList = (from country in r_db.Countries
            //                   select country).AsEnumerable();
            var regionList = r_db.Regions.AsNoTracking().AsEnumerable();
            CountryAndRegion countryRegion = new CountryAndRegion();
            //countryRegion.country = countryList;
            countryRegion.region = regionList;
            return countryRegion;
        }


        internal List<SuppRegion> GetSupRegList()
        {
            List<SuppRegion> suppReg = (from sup in r_db.Suppliers
                          join supreg in r_db.SupplierRegionMappings
                          on sup.SupplierCode equals supreg.SupplierCode
                          join reg in r_db.Regions
                          on supreg.RegionID equals reg.RegionID                          
                          select new SuppRegion
                          {
                              SupplierCode=sup.SupplierCode,
                              SupplierName=sup.SupplierName,
                              SupplierEmailId=sup.SupplierEmailId,
                              SupplierContact=sup.SupplierContact,
                              SupplierAddress1=sup.SupplierAddress1,
                              SupplierAddress2=sup.SupplierAddress2,
                              PinCode=sup.PinCode,
                              City=sup.City,
                              State=sup.State,
                              CountryCode=sup.CountryCode,
                              Country=sup.Country,
                              ContactWithCode=sup.CountryCode+sup.SupplierContact,
                              SupplierIsActive =sup.SupplierIsActive,
                              SupplierRegion=reg.RegionName,
                              SupplierAddBy=sup.SupplierAddBy,
                              SupplierAddDt=sup.SupplierAddDt,
                              SupplierModfBy=sup.SupplierModfBy,
                              SupplierModfDt=sup.SupplierModfDt,
                              SupplierAddByName= r_db.Users.Where(x => x.UserId == sup.SupplierAddBy).FirstOrDefault().UserFirstName + " " + r_db.Users.Where(x => x.UserId == sup.SupplierAddBy).FirstOrDefault().UserLastName,
                              SupplierModfByName= r_db.Users.Where(x => x.UserId == sup.SupplierModfBy).FirstOrDefault().UserFirstName + " " + r_db.Users.Where(x => x.UserId == sup.SupplierModfBy).FirstOrDefault().UserLastName,
                          }).ToList();
            return suppReg;

        }

        internal bool UpdateSupplier(SuppRegion supplier)
        {
            try
            {
                Supplier supp = r_db.Suppliers.Find(supplier.SupplierCode);
                supp.SupplierCode = supplier.SupplierCode;
                supp.SupplierName = supplier.SupplierName;
                supp.SupplierEmailId = supplier.SupplierEmailId;
                supp.SupplierContact = supplier.SupplierContact;
                supp.CountryCode = supplier.CountryCode;
                supp.SupplierAddress1 = supplier.SupplierAddress1;
                supp.SupplierAddress2 = supplier.SupplierAddress2;
                //correct below fields after adding all fields on form.
                supp.City = supplier.City;
                supp.State = supplier.State;
                supp.PinCode = supplier.PinCode;
                supp.Country = supplier.Country;
                supp.SupplierIsActive = supplier.SupplierIsActive;
                //supp.SupplierComments = supplier.SupplierComments;
                supp.SupplierModfDt = DateTime.Now;
                supp.SupplierModfBy= Convert.ToInt32(claims.Where(x => x.Type == ClaimTypes.Sid).FirstOrDefault().Value); ; 
                r_db.Entry(supp).State = EntityState.Modified;
                
                SupplierRegionMapping supregion = r_db.SupplierRegionMappings.Where(sr=>sr.SupplierCode==supplier.SupplierCode).FirstOrDefault();
                supregion.RegionID = r_db.Regions.Where(x => x.RegionName == supplier.SupplierRegion).FirstOrDefault().RegionID;
                supregion.SupplierCode = supplier.SupplierCode;
                r_db.Entry(supregion).State = EntityState.Modified;

                r_db.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                throw;
            }

        }

    }
}