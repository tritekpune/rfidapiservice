﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RfidEntities;

namespace RFIDApiService
{
    public class BaseDAO:IDisposable
    {

        protected RFIDEntities r_db;

        protected User apiUser;

        protected MD5PasswordHasher md5;
        public BaseDAO()
        {
            r_db = new RFIDEntities();
            md5 = new MD5PasswordHasher();
        }

        public BaseDAO(RFIDEntities db)
        {
            r_db = db;
        }

        public BaseDAO(RFIDEntities db, MD5PasswordHasher mdhash5)
        {
            r_db = db;

            md5 = mdhash5;

            //Disable lazy loading to prevent recursion errors on serialization
            db.Configuration.LazyLoadingEnabled = false;
            //db.Configuration.ProxyCreationEnabled = false; 
        }

        public void Dispose()
        {
            r_db.Dispose();
        }
    }
}