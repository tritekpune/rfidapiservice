﻿using RfidEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;

namespace RFIDApiService
{
    public class StackDAO:BaseDAO
    {
        public StackDAO() { }

        protected IEnumerable<Claim> claims;
        public StackDAO(RFIDEntities db)
            : base(db)
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            claims = identity.Claims;
        }

        //internal bool SaveStackDetails(StackDetail stackDetails)
        //{
        //    try
        //    {
        //        stackDetails.StackAddDt = DateTime.Now;
        //        stackDetails.StackAddBy= Convert.ToInt32(claims.Where(x => x.Type == ClaimTypes.Sid).FirstOrDefault().Value);
        //        r_db.StackDetails.Add(stackDetails);
        //        r_db.SaveChanges();
        //        return true;
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }

        //}

        internal List<StackScanTokenDetailModel> GetStackDetailsList()
        {

            List<StackScanTokenDetailModel> stackDetailList = (from stack in r_db.StackScanTokenDetails
                                                      select new StackScanTokenDetailModel
                                                      {
                                                          StackScanId=stack.StackScanId,
                                                          StackScanToken=stack.StackScanToken,
                                                          AssetCount= r_db.RFIDScanDetails.Where(x=>x.StackScanTokenId== stack.StackScanTokenId).Count(),
                                                          StackScanTokenId=stack.StackScanTokenId,
                                                          StackScanAddBy=stack.StackScanAddBy,
                                                          StackScanAddDt=stack.StackScanAddDt,
                                                          StackScanBy=stack.StackScanBy,
                                                          StackAddByName = r_db.Users.Where(x => x.UserId == stack.StackScanAddBy).FirstOrDefault().UserFirstName + " " + r_db.Users.Where(x => x.UserId == stack.StackScanAddBy).FirstOrDefault().UserLastName,
                                                          StackModfByName = r_db.Users.Where(x => x.UserId == stack.StackScanModfBy).FirstOrDefault().UserFirstName + " " + r_db.Users.Where(x => x.UserId == stack.StackScanModfBy).FirstOrDefault().UserLastName,
                                                          StackBarcodeImage = stack.StackBarcodeImage,
                                                          SupplierCode=stack.SupplierCode,
                                                          SupplierName= r_db.Suppliers.Where(x => x.SupplierCode == stack.SupplierCode).FirstOrDefault().SupplierName,
                                                          StackBarCode=stack.StackBarCode,
                                                          StackScanComments=stack.StackScanComments
                                                      }).ToList();
            return stackDetailList;
        }


        /// <summary>
        /// save barcode against the stack
        /// </summary>
        /// <param name="stackBarcode"></param>
        /// <returns></returns>
        internal String SaveStackBarcode(StackScanTokenDetailModel stackBarcode)
        {
            try
            {
                StackScanTokenDetail stack = r_db.StackScanTokenDetails.Where(stk => stk.StackScanToken == stackBarcode.StackScanToken).FirstOrDefault();
                if (stack != null)
                {
                    if (stack.StackScanAccessCount < stackBarcode.StackAcessAllowed)
                    {
                        stack.StackBarcodeImage = stackBarcode.StackBarcodeImage;
                        stack.StackBarCode = stackBarcode.StackBarCode;
                        if (stack.StackScanAccessCount == 0)
                            stack.StackScanAccessCount = 1;
                        else
                            stack.StackScanAccessCount = stack.StackScanAccessCount + 1;
                        r_db.Entry(stack).State = System.Data.Entity.EntityState.Modified;
                        r_db.SaveChanges();
                        return "Stack barcode saved successfully";
                    }
                    else
                        return "Barcode saved for maximum number of allowed count.";
                }
                else
                    return "Stack token inavlid.";
                
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        internal List<RFIDScanDetailModel> SearchStackToken(StackScanTokenDetailModel stackScanTokenDetails)
        {
            List<RFIDScanDetailModel> rfidDetails = (from rfid in r_db.RFIDScanDetails
                                                     join stackscan in r_db.StackScanTokenDetails
                                                     on rfid.StackScanTokenId equals stackscan.StackScanTokenId
                                                     where stackscan.StackScanToken == stackScanTokenDetails.StackScanToken
                                                     select new RFIDScanDetailModel
                                                     {
                                                         StackScanTokenId = stackscan.StackScanTokenId,
                                                         SupplierCode = rfid.SupplierCode,
                                                         SupplierName = r_db.Suppliers.Where(sup => sup.SupplierCode == rfid.SupplierCode).FirstOrDefault().SupplierName,
                                                         RFIDId = rfid.RFIDId,
                                                         ScanAddDt = rfid.ScanAddDt,
                                                         ScanAddBy = rfid.ScanAddBy,
                                                         StackAddByName = r_db.Users.Where(x => x.UserId == rfid.ScanAddBy).FirstOrDefault().UserFirstName + " " + r_db.Users.Where(x => x.UserId == rfid.ScanAddBy).FirstOrDefault().UserLastName,
                                                         ScanModfDt = rfid.ScanModfDt,
                                                         ScanModfBy = rfid.ScanModfBy,
                                                         StackModfByName = r_db.Users.Where(x => x.UserId == rfid.ScanModfBy).FirstOrDefault().UserFirstName + " " + r_db.Users.Where(x => x.UserId == rfid.ScanModfBy).FirstOrDefault().UserLastName,
                                                         RFIDCode = r_db.RFIDs.Where(r => r.RFIDId == rfid.RFIDId).FirstOrDefault().RFIDCode,
                                                         AllowGenerateBarcode = stackscan.StackScanAccessCount < stackScanTokenDetails.StackAcessAllowed ? true:false
                               }).ToList<RFIDScanDetailModel>();
            
            return rfidDetails;
        }

        internal dynamic GetSupplierWiseStackDetails()
        {
            List<SupplierWiseStack> result = r_db.Database.SqlQuery<SupplierWiseStack>("select ss.StackScanTokenId, Max(ss.SupplierCode) as SupplierCode,count(rfid.StackScanTokenId) as AssetCount, " +
                " Max(sup.SupplierName) as SupplierName,Max(ss.StackScanToken) as StackToken,(select UserFirstName+' '+UserLastName  from [dbo].[Users] where userid=ss.StackScanBy) as StackScanBy,Max(ss.StackScanAddDt) as StackScanDate from [dbo].[Suppliers] sup " +
                " right join[dbo].[StackScanTokenDetails] ss "+
                " on sup.SupplierCode = ss.SupplierCode "+
                " inner join[dbo].[RFIDScanDetails] rfid "+
                " on rfid.StackScanTokenId = ss.StackScanTokenId group by ss.StackScanTokenId,StackScanBy").ToList();
            return result;
        }

        internal dynamic GetUserWiseStackDetails()
        {
            dynamic result = (from stack in r_db.StackScanTokenDetails.AsNoTracking()
                         from user in r_db.Users
                             .Where(user => user.UserId == stack.StackScanAddBy).DefaultIfEmpty()
                         select new
                         {
                             StackScanId= stack.StackScanId,
                             StackScanToken=stack.StackScanToken,
                             StackScanTokenId=stack.StackScanTokenId,
                             StackScanTokenIsActive= stack.StackScanTokenIsActive,
                             StackScanAddDt=stack.StackScanAddDt,
                             StackScanAddBy=stack.StackScanAddBy,
                             StackScanAddByName=user.UserFirstName+" "+user.UserLastName,
                             SupplierCode=stack.SupplierCode,
                             SupplierName= r_db.Suppliers.Where(sup => sup.SupplierCode == stack.SupplierCode).FirstOrDefault().SupplierName,
                             UserRole =r_db.Roles.Where(role=>role.RoleId==user.UserRole).FirstOrDefault().RoleName,
                             UserEmailId=user.UserEmailId,
                             UserContact=user.UserContact,
                             ContactWithCode = user.UserCountryCode + user.UserContact,
                         }).ToList();
            return result;
        }

    }
}