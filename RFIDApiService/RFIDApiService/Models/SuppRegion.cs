﻿using RfidEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RFIDApiService.Models
{
    public class SuppRegion
    {
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string SupplierEmailId { get; set; }
        public string SupplierContact { get; set; }
        public string ContactWithCode { get; set; }
        public string SupplierAddress1 { get; set; }
        public string SupplierAddress2 { get; set; }
        public string PinCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string CountryCode { get; set; }
        public string Country { get; set; }
        public bool SupplierIsActive { get; set; }
        public string SupplierRegion { get; set; }

        public Nullable<System.DateTime> SupplierAddDt { get; set; }
        public Nullable<int> SupplierAddBy { get; set; }
        public String SupplierAddByName { get; set; }
        public Nullable<System.DateTime> SupplierModfDt { get; set; }
        public Nullable<int> SupplierModfBy { get; set; }
        public String SupplierModfByName { get; set; }

    }
}