﻿using RfidEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RFIDApiService.Models
{
    public class EditRole
    {
        public int userId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailId { get; set; }
        public string Contact { get; set; }
        public string CountryCode { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> AddDt { get; set; }
        public Nullable<System.DateTime> ModfDt { get; set; }
        public string AddBy { get; set; }
        public string ModfBy { get; set; }
        public bool IsAdd { get; set; }

    }

    public class CountryAndRegion
    {
        public IEnumerable<Country> country { get; set; }
        public IEnumerable<Region> region { get; set; }
    }
}