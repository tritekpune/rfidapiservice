﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RFIDApiService.Models
{
    public class LoginUser
    {
       
            public int UserID { get; set; }

            [Required(ErrorMessage = "Required Username")]
            [StringLength(30, MinimumLength = 2, ErrorMessage = "Username Must be Minimum 2 Charaters")]
            public string UserName { get; set; }

            [DataType(DataType.Password)]
            [Required(ErrorMessage = "Required Password")]
            [MaxLength(30, ErrorMessage = "Password cannot be Greater than 30 Charaters")]
            [StringLength(31, MinimumLength = 7, ErrorMessage = "Password Must be Minimum 7 Charaters")]
            public string Password { get; set; }

            [Required(ErrorMessage = "Required EmailID")]
            [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter Valid Email ID")]
            public string EmailID { get; set; }
        
    }


    public class ResetPassword
    {
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public string ReturnToken { get; set; }
    }


    public class AuthUser
    {
        public int userId { get; set; }
        public string userName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailId { get; set; }
        public string RoleName { get; set; }
        public bool isActive { get; set; }
    }
}