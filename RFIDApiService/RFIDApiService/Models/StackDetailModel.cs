﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RFIDApiService
{
    public partial class StackScanTokenDetailModel
    {
        public int StackScanId { get; set; }
        public string StackScanToken { get; set; }
        public int StackScanBy { get; set; }
        public string StackScanTokenId { get; set; }
        public Nullable<bool> StackScanTokenIsActive { get; set; }
        public Nullable<System.DateTime> StackScanAddDt { get; set; }
        public Nullable<int> StackScanAddBy { get; set; }
        public string StackAddByName { get; set; }
        public Nullable<int> StackScanModfDt { get; set; }
        public Nullable<int> StackScanModfBy { get; set; }
        public byte[] StackBarcodeImage { get; set; }
        public string StackModfByName { get; set; }
        public int AssetCount { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string StackBarCode { get; set; }
        public string StackScanComments { get; set; }
        public int StackScanAccessCount { get; set; }
        public int StackAcessAllowed { get; set; }
    }

    public class RFIDScanDetailModel
    {
        public string StackScanTokenId { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public int RFIDId { get; set; }
        public Nullable<System.DateTime> ScanAddDt { get; set; }
        public Nullable<int> ScanAddBy { get; set; }
        public string StackAddByName { get; set; }
        public Nullable<System.DateTime> ScanModfDt { get; set; }
        public Nullable<int> ScanModfBy { get; set; }
        public string StackModfByName { get; set; }
        public string RFIDCode { get; set; }

        public bool AllowGenerateBarcode { get; set; }
    }

    public class SupplierWiseStack
    {
        public string StackScanTokenId { get; set; }
        public string SupplierCode { get; set; }
        public int AssetCount { get; set; }
        public string SupplierName { get; set; }
        public string StackToken { get; set; }
        public string StackScanBy { get; set; }
        public DateTime? StackScanDate { get; set; }

    }
}