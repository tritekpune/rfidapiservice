﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RFIDApiService
{
    public class RFIDModel
    {
        public int RFIDId { get; set; }
        public string RFIDCode { get; set; }
        public string RFIDSupplierName { get; set; }
        public string RFIDSupplierCode { get; set; }
        public bool RFIDIsActive { get; set; }
        public string RFIDComments { get; set; }
        public Nullable<System.DateTime> RFIDAddDt { get; set; }
        public Nullable<int> RFIDAddBy { get; set; }
        public string RFIDAddByName { get; set; }
        public Nullable<System.DateTime> RFIDModfDt { get; set; }
        public Nullable<int> RFIDModfBy { get; set; }
        public string RFIDModfByName { get; set; }

    }
}