//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RfidEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Supplier
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Supplier()
        {
            this.SupplierRegionMappings = new HashSet<SupplierRegionMapping>();
        }
    
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string SupplierEmailId { get; set; }
        public string SupplierContact { get; set; }
        public string SupplierAddress1 { get; set; }
        public string SupplierAddress2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PinCode { get; set; }
        public string Country { get; set; }
        public bool SupplierIsActive { get; set; }
        public string SupplierComments { get; set; }
        public Nullable<System.DateTime> SupplierAddDt { get; set; }
        public Nullable<int> SupplierAddBy { get; set; }
        public Nullable<System.DateTime> SupplierModfDt { get; set; }
        public Nullable<int> SupplierModfBy { get; set; }
        public int SupplierId { get; set; }
        public string CountryCode { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SupplierRegionMapping> SupplierRegionMappings { get; set; }
    }
}
